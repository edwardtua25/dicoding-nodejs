const {
  addNoteHandler,
  getAllNoteHandler,
  getNoteByIdHandler,
  editNoteByIdHandler,
  deleteNoteByIdHandler,
} = require('./handler');

/* eslint-disable key-spacing */
const routes = [
  {
    method:'POST',
    path: '/notes',
    handler: addNoteHandler,
    options:{
      cors:{
        origin: ['*'],
      },
    },

  },
  {
    method: 'GeT',
    path:'/notes',
    handler: getAllNoteHandler,
  },

  {
    method: 'GET',
    path:'/notes/{id}',
    handler:getNoteByIdHandler,
  },
  {
    method: 'PUT',
    path: '/notes/{id}',
    handler: editNoteByIdHandler,
  },
  {
    method: 'DELETE',
    path: '/notes/{id}',
    handler: deleteNoteByIdHandler,
  },
];

module.exports = routes;
